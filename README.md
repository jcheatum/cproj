# CProj

CProj is a simple cli tool for creating and managing C projects.

```
Usage: cproj [command] [options] [arguments]
Commands:
    help                            Print this help message and exit.
    new [options] [NAME]            Create a new project with the given 
                                    name.
    rename [options] [NEW_NAME]     (Deprecated) Rename the project located in 
                                    the current working directory.
    edit [options]                  Edit project configuration.
For more detailed information on each command run `cproj <command> --help`
```

## Installation

### Arch Linux

CProj is available on the AUR as [cproj-git](https://aur.archlinux.org/packages/cproj-git)

### Other

Clone or download the repository and run the following command:

```sh
$ pip install /path/to/cproj
```

## Project Structures

A project created with CProj will have one of the following file structures:

Binary:
```
<project name>/
├── .cproj
├── .git/
├── .gitignore
├── LICENSE
├── Makefile
└── src/
    └── main.c
```

Library:
```
<project name>/
├── .cproj
├── .git/
├── .gitignore
├── LICENSE
├── Makefile
└── src/
    ├── lib.c
    └── <project name>.h
```

## The .cproj File

The .cproj file is a yaml file containing information about the project. An
example is shown below:

```yaml
!ProjectProperties
license: null
name: test-lib
type: !!python/object/apply:cproj.__main__.ProjType
- 1
use_git: true
```

In general, it is best to avoid editing this file directly and instead let CProj
do all the work.

## License

CProj is free software, released under the Mozilla Public License Version 2.0.
