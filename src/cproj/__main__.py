#!/usr/bin/env python

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import os, sys, getopt, re, copy
from enum import Enum

import spdx_lookup, yaml

class ProjType(Enum):
    BIN = 0
    LIB = 1

    def __str__(self):
        if self == BIN:
            return 'bin'
        else:
            return 'lib'

class ProjectProperties(yaml.YAMLObject):
    yaml_tag = '!ProjectProperties'
    def __init__(self, name: str, license: str = None, type: ProjType = ProjType.BIN, use_git: bool = True):
        self.name = name
        self.license = license
        self.type = type
        self.use_git = use_git
    
    def __repr__(self):
        return '%s(name=%r, license=%r, type=%r, use_git=%r)' % (
            self.__class__.__name__, self.name, self.license, self.type, self.use_git
        )

def print_help():
    print('''Usage: cproj [command] [options] [arguments]
Commands:
    help                            Print this help message and exit.
    new [options] [NAME]            Create a new project with the given 
                                    name.
    rename [options] [NEW_NAME]     (Deprecated) Rename the project located in 
                                    the current working directory.
    edit [options]                  Edit project configuration.
For more detailed information on each command run `cproj <command> --help`''')

def print_create_help():
    print('''Usage: cproj new [options] [NAME]
Options:
    -h, --help          Print this help message and exit.
    -b, --bin           Create a binary application (default).
    -l, --lib           Create a shared library.
    -L, --license ID    Add a LICENSE file with the license text 
                        corresponding to the given SPDX identifier
                        (default: no license, all rights reserved).
    -g, --no-git        Do not automatically create a git repository and
                        .gitignore file for this project.''')

def print_rename_help():
    print('''Usage: cproj rename [options] [NEW_NAME]
Options:
    -h, --help      Print this help message and exit.''')

def print_edit_help():
    print('''Usage: cproj edit [options]
Options:
    -h, --help              Print this help message and exit.
    -t, --type PROJ_TYPE    Change the type of the project to PROJ_TYPE. 
                            Accepted values are "bin" and "lib" 
                            (case-insensitive).
    -L, --license ID        Change the license of the project to the given SPDX
                            identifier.
    -n, --name NAME         Change the name of the project to NAME.
    -y, --no-confirm        Do not confirm changes.
''')

# Hello World in C, default main.c source for bin projects
C_BIN_MAIN = '''#include <stdio.h>

int main(int argc, char **argv) {
    printf("Hello, World!\\n");
    return 0;
}
'''

def main():
    if len(sys.argv) < 2:
        print_help()
        exit(1)

    argv = sys.argv[2:]
    command = sys.argv[1]

    if command == 'new':
        # Various information about the project
        properties = ProjectProperties(None)
        type_set = False

        try:
            opts, args = getopt.getopt(argv, 'hblL:g', ['help', 'bin', 'lib', 'license=', 'no-git'])
        except:
            print('cproj: Error parsing arguments', file=sys.stderr)

        for opt, arg in opts:
            if opt in ['-h', '--help']:
                print_help()
                exit(0)
            elif opt in ['-b', '--bin']:
                if not type_set:
                    properties.type = ProjType.BIN
                    type_set = True
                else:
                    print(f'cproj: {command}: Error: Project can only have one type', file=sys.stderr)
                    exit(1)
            elif opt in ['-l', '--lib']:
                if not type_set:
                    properties.type = ProjType.LIB
                    type_set = True
                else:
                    print(f'cproj: {command}: Error: Project can only have one type', file=sys.stderr)
                    exit(1)
            elif opt in ['-L', '--license']:
                properties.license = arg
            elif opt in ['-g', '--no-git']:
                use_git = False
            else:
                print(f'cproj: {command}: Error: Option \'{opt}\' not recognized', file=sys.stderr)
                exit(1)
        
        if len(args) == 1:
            properties.name = args[0]
        elif len(args) < 1:
            print(f'cproj: {command}: Error: Missing project name', file=sys.stderr)
            print_create_help()
            exit(1)
        else:
            print(f'cproj: {command}: Error: Too many arguments')
            print_create_help()
            exit(1)

        new_project(properties)
    elif command == 'rename':
        try:
            opts, args = getopt.getopt(argv, 'h', ['help'])
        except:
            print('cproj: Error parsing arguments', file=sys.stderr)

        print('Warning: This command is deprecated. Use `cproj edit --name` instead.')
        for opt, arg in opts:
            if opt in ['-h', '--help']:
                print_rename_help()
                exit(0)
        
        if len(args) == 1:
            name = args[0]
        elif len(args) < 1:
            print(f'cproj: {command}: Error: Missing project new name', file=sys.stderr)
            print_rename_help()
            exit(1)
        else:
            print(f'cproj: {command}: Error: Too many arguments')
            print_rename_help()
            exit(1)
        
        rename_project(name)
    elif command == 'edit':
        try:
            opts, args = getopt.getopt(argv, 'ht:L:n:y', ['help', 'type=', 'license=', 'name=', 'no-confirm'])
        except:
            print('cproj: Error parsing arguments', file=sys.stderr)

        if len(argv) == 0:
            print_edit_help()
            exit(1)

        noconfirm = False
        for opt, arg in opts:
            if opt in ['-h', '--help']:
                print_edit_help()
                exit(0)
            elif opt in ['-y', '--no-confirm']:
                noconfirm = True
        
        if not os.path.exists('.cproj'):
            print('cproj: edit: This directory does not contain a cproj project', file=sys.stderr)
            print_edit_help()
            exit(1)
        else:
            with open('.cproj', 'r') as config_file:
                config = yaml.load(config_file, yaml.Loader)
            if type(config) is dict:
                if not noconfirm:
                    confirm = input('This project has a .cproj file from an older version. Update to current version? [Y/n]: ')
                    if confirm in ['n', 'N', 'no', 'No', 'NO']:
                        print('Aborting')
                        exit(1)
                properties = ProjectProperties(config['name'])

                if os.path.exists('LICENSE'):
                    with open('LICENSE', 'r') as l:
                        properties.license = spdx_lookup.match(l.read()).license.name
                else:
                    properties.license = None
                
                if os.path.exists('.git'):
                    properties.use_git = True
                else:
                    properties.use_git = False

                with open('Makefile', 'r') as mkfile:
                    if re.match(r'lib[a-z0-9_]*\.so:', mkfile.read()) != None:
                        properties.type = ProjType.LIB
                    else:
                        properties.type = ProjType.BIN
            elif type(config) is ProjectProperties:
                properties = config

        old_props = copy.deepcopy(properties)
        for opt, arg in opts:
            if opt in ['-t', '--type']:
                if arg.lower() == 'bin':
                    properties.type = ProjType.BIN
                elif arg.lower() == 'lib':
                    properties.type = ProjType.LIB
                else:
                    print(f'cproj: edit: Error: Unknown project type "{arg}".', file=sys.stderr)
                    print_edit_help()
                    exit(1)
            elif opt in ['-L', '--license']:
                if arg.lower() == 'none':
                    properties.license = None
                else:
                    properties.license = arg
            elif opt in ['-n', '--name']:
                properties.name = arg
                
        edit_project(old_props, properties, noconfirm)
    elif command in ['help', '-h', '--help']:
        print_help()
        exit(0)
    else:
        print(f'cproj: Error: Command \'{command}\' not recognized', file=sys.stderr)
        print_help()
        exit(1)

def new_project(properties: ProjectProperties):
    # Create project directory structure
    os.mkdir(f'./{properties.name}')
    os.mkdir(f'./{properties.name}/src')
    os.chdir(f'./{properties.name}')

    # If using git, setup git repository with .gitignore
    if properties.use_git: 
        ignore = f'*.o\n{properties.name if properties.type == ProjType.BIN else f"lib{properties.name}.so"}\n.cproj\n.vscode/\n'
        with open('.gitignore', 'w') as gitignore:
            gitignore.write(ignore)
        os.system('git init')

    # Create LICENSE file if an SPDX license identifier was given
    if properties.license != None:
        lic = spdx_lookup.by_id(properties.license)
        if lic == None:
            print(f'cproj: new: Error: License ID \'{properties.license}\' not found in SPDX database', file=sys.stderr)
        else:
            with open('LICENSE', 'w') as license_file:
                license_file.write(lic.template)

    if properties.type == ProjType.BIN:
        # Makefile content for bin project
        make = f'.PHONY: clean\n\n{properties.name}: main.o\n\t$(CC) $^ -o $@\n\n%.o: src/%.c\n\t$(CC) -c $< -o $@\n\nclean:\n\trm -f *.o {properties.name}\n'

        # Create initial main.c source file
        with open('src/main.c', 'w') as main_c:
            main_c.write(C_BIN_MAIN)
    else:
        # Makefile content for lib project
        make = f'CFLAGS += -fPIC -c\n\n.PHONY: clean\n\nlib{properties.name.lower().replace("-", "_")}.so: lib.o\n\t$(CC) -shared $^ -o $@\n\n%.o: src/%.c\n\t$(CC) $(CFLAGS) $< -o $@\n\nclean:\n\trm -f *.o lib{properties.name.lower().replace("-", "_")}.so\n'

        C_LIB_H = f'#ifndef _{properties.name.upper().replace("-", "_")}_H\n#define _{properties.name.upper().replace("-", "_")}_H\n\nint do_something();\n\n#endif\n'
        C_LIB = f'#include "{properties.name.lower().replace("-", "_")}.h"\n\nint do_something() {{\n    // TODO: Do something\n    return 0;\n}}\n'

        # Create initial lib.c and lib.h source files
        with open('src/lib.c', 'w') as lib_c:
            lib_c.write(C_LIB)

        with open(f'src/{properties.name.lower().replace("-", "_")}.h', 'w') as lib_h:
            lib_h.write(C_LIB_H)
            lib_h.close()
    # Create makefile
    with open('Makefile', 'w') as makefile:
        makefile.write(make)
    # Create .cproj file so we know this project was created by this script
    with open('.cproj', 'w') as config_file:
        yaml.dump(properties, config_file)
           
def rename_project(new_name: str):
    if not os.path.exists('.cproj'):
        print('cproj: rename: This directory does not contain a cproj project', file=sys.stderr)
        print_rename_help()
        exit(1)
    else:
        with open('.cproj', 'r') as config_file:
            config = yaml.load(config_file, yaml.Loader)
        with open('Makefile', 'r') as makefile:
            make = makefile.read()
        # Maintain compatibility with old .cproj files
        if type(config) is dict:
            make = re.sub(f'{config["name"]}', new_name, make)
            config['name'] = new_name
        elif type(config) is ProjectProperties:
            make = re.sub(f'{config.name}', new_name, make)
            config.name = new_name
        with open('Makefile', 'w') as makefile:
            makefile.write(make)
        with open('.cproj', 'w') as config_file:
            yaml.dump(config, config_file)

def edit_project(old_props: ProjectProperties, new_props: ProjectProperties, noconfirm=False):
    if not noconfirm:
        prompt = 'The following changes will be made:\n'
        if old_props.name != new_props.name:
            prompt += f'\tname: {old_props.name}\t->\t{new_props.name}\n'
        if old_props.license != new_props.license:
            prompt += f'\tlicense: {old_props.license}\t->\t{new_props.license}\n'
        prompt += 'Confirm [Y/n]: '
        confirm = input(prompt)
        if confirm in ['n', 'N', 'no', 'No', 'NO']:
            print('Aborting')
            exit(1)

    if old_props.name != new_props.name:
        with open('Makefile', 'r') as makefile:
            make = makefile.read()
        make = re.sub(f'{old_props.name}', new_props.name, make)
        with open('Makefile', 'w') as makefile:
            makefile.write(make)

    if old_props.license != new_props.license and new_props.license != None:
        lic = spdx_lookup.by_id(new_props.license)
        if lic == None:
            print(f'cproj: new: Error: License ID \'{new_props.license}\' not found in SPDX database', file=sys.stderr)
        else:
            with open('LICENSE', 'w') as license_file:
                license_file.write(lic.template)
    elif new_props.license == None and os.path.exists('LICENSE'):
        os.remove('LICENSE')

    with open('.cproj', 'w') as config_file:
        yaml.dump(new_props, config_file)

if __name__ == '__main__':
    main()